//cb =(reduced,value)= reduced+=value
module.exports={
    reduce: function(elements, cb, startingValue){
        let index=startingValue || 0;
        let reduced= elements[index];
        //let reduced=c;
        for(let i=index+1;i<elements.length;i++){
            reduced=cb(reduced,elements[i]);
        }
        return reduced;
    }
}