//let arr=[]
module.exports={
flatten: function(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    let arr=[];
    function rarr(item){
        Array.isArray(item)?check(item):arr.push(item);
    }
    function check(arr){
        for(let val of arr){
            //console.log(val)
            rarr(val);

        }
    }
    check(elements);
    return arr;
}
}